#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================

from RenderViewMouseInteractor import *
import cinema_python.images.camera_utils as camera_utils
import math

class ArcBallInteractor(RenderViewMouseInteractor):
    """
    mouse interaction for azimuth-elevation-roll and yaw-pitch-roll type
    stores.
    What we do is map mouse press locations on the screen onto the
    surface of a sphere. On mouse drag we move the sphere accordingly.
    i.e. an Arcball and keep track of it's orientation matrix.
    In cinema, we store the orientation matrices of each image, and so we
    then just find the closest orientation matrix in the store to the arball
    to decide what to show.
    """

    newPose = Signal(int)

    def __init__(self):
        super(ArcBallInteractor, self).__init__()
        self.posenum = 0
        self.mnow = None
        self._defaultPose = None
        self._poses = None
        self._xy = None

    def onMouseRelease(self, mouseEvent):
        self._defaultPose = self.mnow
        self.mnow = None
        super(ArcBallInteractor, self).onMouseRelease(mouseEvent)

    def initializePoses(self, param):
        self._poses = param["values"]
        self._defaultPose = self._poses[0]
        if "default" in param:
            self._defaultPose = param["default"]
        self.posenum = self._poses.index(self._defaultPose)

    def updateCamera(self):
        self.newPose.emit(self.posenum)

    def mouseMove(self, x,y, dx, dy):
        if x < 0: x = 0
        if y < 0: y = 0

        maxW = float(self.parent().frameRect().width())
        maxH = float(self.parent().frameRect().height())
        if x > maxW: x = maxW
        if y > maxH: y = maxH

        #where are we looking now
        if self.mnow == None:
            self.mnow = self._defaultPose
            self.pi = self.win_to_sph([x/maxW, y/maxH], [0.5,0.5], 1.0)
            self._xy = (x, y)
            return
        else:
            pn = self.win_to_sph([x/maxW, y/maxH], [0.5,0.5], 1.0)
            self._xy = (x, y)

        xp = self.cross_product(self.pi, pn)
        dot = self.dot_product(self.pi, pn)
        angle = math.acos(dot)/(math.pi)
        mag = math.sqrt(xp[0]*xp[0]+xp[1]*xp[1]+xp[2]*xp[2])
        if mag == 0.0:
            return
        for i in range(3):
            xp[i] /= mag
        q = [xp[0], xp[1], xp[2], angle]
        qM = self.quat_to_rot_mtx(q)
        self.pi = pn

        #rotate camera
        mnext = self.matrix_mul(self.mnow, qM)
        self.mnow = mnext

        self.posenum = camera_utils.nearest_camera(self._poses, mnext)

    @staticmethod
    def cross_product(x, y):
        return [x[1]*y[2] - x[2]*y[1],
                x[2]*y[0] - x[0]*y[2],
                x[0]*y[1] - x[1]*y[0]];

    @staticmethod
    def dot_product(x, y):
        def do_elem(s,e):
            return s+e[0]*e[1]
        return reduce(do_elem, map(None,x,y), 0.0)

    @staticmethod
    def matrix_mul(mtx_a, mtx_b):
        tpos_b = zip( *mtx_b)
        rtn = [[ sum( ea*eb for ea,eb in zip(a,b)) for b in tpos_b] for a in mtx_a]
        return rtn

    @staticmethod
    def quat_to_rot_mtx(q):
        """
        takes in a vector and an angle of rotation around the vector
        returns the corresponding 3x3 transformation matrix
        """
        mat = [[0.0]*4,[0.0]*4,[0.0]*4,[0.0]*4]
        x, y, z, w = q

        rcos = math.cos(w)
        rsin = math.sin(w)

        u = x
        v = y
        w = z

        mat[0][0] =      rcos + u*u*(1-rcos);
        mat[1][0] =  w * rsin + v*u*(1-rcos);
        mat[2][0] = -v * rsin + w*u*(1-rcos);
        mat[0][1] = -w * rsin + u*v*(1-rcos);
        mat[1][1] =      rcos + v*v*(1-rcos);
        mat[2][1] =  u * rsin + w*v*(1-rcos);
        mat[0][2] =  v * rsin + u*w*(1-rcos);
        mat[1][2] = -u * rsin + v*w*(1-rcos);
        mat[2][2] =      rcos + w*w*(1-rcos);

        mat[3][3] = 1

        return mat

    @staticmethod
    def win_to_sph(pnt_win, sph_center, radius):
        """
        project a mouse click in the window onto the sphere.
        Points beyond the sphere radius (window corners for example) map to
        the outside edge of the sphere and are useful for rolling the camera.
        """
        pnt = [pnt_win[0]-sph_center[0], pnt_win[1]-sph_center[1], 0.0]
        r = math.sqrt(pnt[0]*pnt[0] + pnt[1]*pnt[1])
        if r > 0.5:
            # flipping x,y to keep appearance of rotating object
            # otherwise, l, r seem OK, but r seems backward
            t = pnt[0]
            pnt[0] = pnt[1] / math.sqrt(r)
            pnt[1] = t / math.sqrt(r)
            pnt[2] = 0
        else:
            pnt[2] = math.sqrt(1-r)
        return pnt
