#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
from PySide import QtCore
from PySide.QtCore import *

import math

class RenderViewMouseInteractor(QObject):
    """ base class for items that map mouse motions in a renderview
    into requests for images from a different viewpoint from the store """
    NoneState   = 0
    RotateState = 1
    PanState    = 2
    ZoomState   = 3

    def __init__(self, parent = None):
        super(RenderViewMouseInteractor, self).__init__(parent)
        self._state = self.NoneState
        self._xy = None

        # Current camera settings
        self._scale = 1

    def getScale(self):
        return self._scale

    @QtCore.Slot('QMouseEvent')
    def onMousePress(self, mouseEvent):
        if (mouseEvent.button() == Qt.LeftButton):
            self._xy = (mouseEvent.x(), mouseEvent.y())
            self._state = self.RotateState
        elif (mouseEvent.button() == Qt.RightButton):
            self._state = self.ZoomState
            self._xy = (mouseEvent.x(), mouseEvent.y())

    @QtCore.Slot('QMouseEvent')
    def onMouseMove(self, mouseEvent):
        if (self._xy == None):
            return

        dx = mouseEvent.x() - self._xy[0]
        dy = mouseEvent.y() - self._xy[1]

        if (self._state == self.RotateState):
            self.mouseMove(mouseEvent.x(), mouseEvent.y(), dx,dy)

        elif (self._state == self.ZoomState):
            scaleFactor = 1.01
            if (dy < 0):
                for i in range(-dy):
                    self._scale = self._scale * scaleFactor
            else:
                for i in range(dy):
                    self._scale = self._scale * (1.0 / scaleFactor)
            self._xy = (mouseEvent.x(), mouseEvent.y())

    @QtCore.Slot('QMouseEvent')
    def onMouseRelease(self, mouseEvent):
        self._xy = None

    @QtCore.Slot('QWheelEvent')
    def onMouseWheel(self, event):
        scaleFactor = 1.01
        # reduce the size of delta for more controllable zooming
        dy = event.delta() / 20
        if (dy > 0):
            for i in range(dy):
                self._scale = self._scale * scaleFactor
        else:
            for i in range(-dy):
                self._scale = self._scale * (1.0 / scaleFactor)

    def updateCamera(self):
        # a chance to fire off final events
        print "WARNING something is wrong child class should be running"
        pass
