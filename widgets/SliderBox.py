#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
from PySide import QtCore
from PySide.QtCore import *
from PySide.QtGui import *
from widgets.uiFiles import ui_SliderBox as ui

benchmarkme=False
if benchmarkme:
    import time

class SliderBox(QGroupBox):
    # Qt signals in PySide style
    queryChanged = Signal() # intended to trigger rendering events
    parameterChanged = Signal(int) # intended to synchronize with another controller (e.g. interactor)

    def __init__(self, parent):
        super(SliderBox, self).__init__(parent)
        self._ui = ui.Ui_SliderBox()
        self._values = None
        self._parameterName = None
        self._query = None
        self._playTimer = QTimer(self)

        self._ui.setupUi(self)
        self._playTimer.setInterval(5) #arbitrarily chosen small number
        self.hide()

        # default theme icons are specified directly in the ui_SliderBox.ui, for details see:
        # - http://5in4.de/blog/2012/02/07/using-standard-icons-in-qt-designer-and-qt-creator/
        # - http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html

        # this hack is necessary given that setting default theme icons in *.ui
        # does not currently work
        self._ui._pbPlay.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self._ui._pbSeekBackward.setIcon(self.style().standardIcon(QStyle.SP_MediaSeekBackward))
        self._ui._pbSkipBackward.setIcon(self.style().standardIcon(QStyle.SP_MediaSkipBackward))
        self._ui._pbSeekForward.setIcon(self.style().standardIcon(QStyle.SP_MediaSeekForward))
        self._ui._pbSkipForward.setIcon(self.style().standardIcon(QStyle.SP_MediaSkipForward))

        # internal connection to update label
        self._ui._sliderValue.valueChanged.connect(self._onValueChanged)
        self._ui._pbSeekBackward.clicked.connect(self._onSeekBackward)
        self._ui._pbSkipBackward.clicked.connect(self._onSkipBackward)
        self._ui._pbSeekForward.clicked.connect(self._onSeekForward)
        self._ui._pbSkipForward.clicked.connect(self._onSkipForward)
        self._ui._pbPlay.toggled.connect(self._onPlayToggled)
        self._playTimer.timeout.connect(self._onPlayTimerUpdate)

    def _onValueChanged(self, index):
        value = self._values[index]
        if self._ui._laValue.text() == '{0}'.format(value):
            return
        self._ui._laValue.setText('{0}'.format(value))
        self._generateQuery(value)
        self.queryChanged.emit()
        self.parameterChanged.emit(value)

    def _onSeekBackward(self):
        sl = self._ui._sliderValue
        minValue = sl.minimum()
        sl.setValue(minValue if sl.value() == minValue else sl.value() - 1)

    def _onSkipBackward(self):
        sl = self._ui._sliderValue
        sl.setValue(sl.minimum())

    def _onSeekForward(self):
        sl = self._ui._sliderValue
        maxValue = sl.maximum()
        sl.setValue(maxValue if sl.value() == maxValue else sl.value() + 1)

    def _onSkipForward(self):
        sl = self._ui._sliderValue
        sl.setValue(sl.maximum())

    @QtCore.Slot(bool)
    def _onPlayToggled(self, status):
        if status:
#          if benchmarkme:
#              self.t0 = time.time()
          # reset if starting from maximum
          sl = self._ui._sliderValue
          if sl.value() == sl.maximum():
              sl.setValue(0)

          self._playTimer.start()

        else:
          self._playTimer.stop()

    @QtCore.Slot()
    def _onPlayTimerUpdate(self):
        sl = self._ui._sliderValue
        if sl.value() < sl.maximum():
            sl.setValue(sl.value() + 1)
        else:
#            if benchmarkme:
#                print time.time()-self.t0
            self._ui._pbPlay.setChecked(False) # triggers _onPlayToggled

    def _generateQuery(self, value):
        self._query = {self._parameterName : set([value])}

    def onParameterChanged(self, value):
        if value in self._values:
            index = self._values.index(value)
            # valueChanged is only emitted if the value is different (prevents
            # infinite loop in cyclic connections)
            self._ui._sliderValue.setValue(index)

    def configureSlider(self, parameters, name):
        self.show()
        self._parameterName = name
        parameter = parameters[name]
        self._values = parameter['values']
        self._ui._sliderValue.setRange(0, len(self._values) - 1)
        self._ui._sliderValue.setPageStep(1)
        defaultValue = parameter['default'] if ('default' in parameter) else self._values[0]
        index = self._values.index(defaultValue)
        self._ui._sliderValue.setValue(index + 1) #hack to update label on init (only emits signal if value is different)
        self._ui._sliderValue.setValue(index)
        self._generateQuery(defaultValue)

    def getQuery(self):
        return self._query
