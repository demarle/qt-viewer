#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
from PySide.QtCore import *
from PySide import QtCore

from common.CinemaSpec import *
from PropertyModel import *
from CinemaParser import *
from PipelineItem import *


class PipelineModel(QAbstractItemModel):
    ''' Data model for top-level pipeline-items to be displayed in a QTreeView.
    This class is intended to be able to populate its items from a parameter_list of
    a valid cinema_store. Each item of the model contains information of its represented
    parameter (such as current color lut, current geometry color, parameter name, etc.).

    The methods ::populate and ::_addItem and ::_isControlValue need to be compliant
    with the cinema database format (reimplement in case of a change).
    '''
    queryChanged = Signal()
    valueRangeChanged = Signal(tuple)
    currentColorLutChanged = Signal(str)
    itemVisibilityChanged = Signal(bool)

    ##################### QAbstractItemModel API ##############################

    def __init__(self, parent = None):
        super(PipelineModel, self).__init__(parent)
        self._rootItem = None
        self._query = None
        self.__defaultClut = None
        self.__colorDefinitions = {}
        self.__parser = None

        rootData = ["Layer Description", "Visible"]
        self._rootItem = PipelineItem(rootData)

    def data(self, index, role):
        if not index.isValid():
            return None

        if role == Qt.DisplayRole:
            item = index.internalPointer()
            return item.data(index.column())

        return None

    def setData(self, index, value, role):
        item = index.internalPointer()
        item.setData(index.column(), value)
        self.dataChanged.emit(index, index)
        return True

    def flags(self, index):
        if not index.isValid():
            return 0;

        return Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def headerData(self, section, orientation, role):
        if (orientation == Qt.Horizontal and role == Qt.DisplayRole):
            return self._rootItem.data(section)

        return None

    def index(self, row, column, parent):
        if not self.hasIndex(row, column, parent):
            return QModelIndex()

        if not parent.isValid():
            parentItem = self._rootItem
        else:
            parentItem = parent.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QModelIndex()

    def parent(self, index):
        if not index.isValid():
            return QModelIndex()

        childItem = index.internalPointer()
        parentItem = childItem.parent()

        if parentItem is self._rootItem or parentItem == None:
            return QModelIndex()

        return self.createIndex(parentItem.row(), 0, parentItem)

    def rowCount(self, parent):
        if parent.column() > 0:
            return 0;

        if not parent.isValid():
            parentItem = self._rootItem
        else:
            parentItem = parent.internalPointer()

        return parentItem.childCount()

    def columnCount(self, parent):
        if parent.isValid():
            return parent.internalPointer().columnCount();
        else:
            return self._rootItem.columnCount()

    def sort(self, column, order):
        if column is not 0:
          raise ValueError("Only displayName sorting is supported!")

        compareKey = lambda x: x._displayData
        if order is Qt.AscendingOrder:
          rev = False
        elif order is Qt.DescendingOrder:
          rev = True

        pipelineItems = self._rootItem._childItems
        pipelineItems.sort(key = compareKey, reverse = rev)

        for item in pipelineItems:
          parameters = item._childItems
          parameters.sort(key = compareKey, reverse = rev)

          for param in parameters:
            values = param._childItems
            values.sort(key = compareKey, reverse = rev)

        topLeftIndex = self.createIndex(0, 0, self._rootItem)
        bottomRightIndex = self.createIndex(self._rootItem.childCount(), 0, self._rootItem)
        self.dataChanged.emit(topLeftIndex, bottomRightIndex)

    ########################## Model specific #################################

    def setDefaultColorLut(self, clut):
        self.__defaultClut = clut

    def populate(self, store, names, version = CinemaSpec.A):
        ''' Fills up the model with meaningful parameters. All the database control
        entities are ignored. When generating a query from the view, the ignored control
        variables are expected to be handled by the database (see FindForViewer). '''
        if version is CinemaSpec.A:
            self.__parser = CinemaParser_SpecA()
        elif version is CinemaSpec.B:
            self.__parser = CinemaParser_SpecB(self.__defaultClut)
        else:
            raise AttributeError("Unknown Cinema database format!")

        self.__parser.parse(store, names, self._rootItem, {}) # traverses parameter list and populates under _rootItem
        self.__updateColorDefinitions()
        self._initializeQuery()
        self.sort(0, Qt.AscendingOrder)
        self.queryChanged.emit()

    def _initializeQuery(self):
        self._query = self.__parser.initializeQuery(self._rootItem)

    def __updateColorDefinitions(self):
        ''' Updates a dictionary containing the current user defined colors (stored
        in the PipelineModel). This dictionary is then queried within compositor.py to
        get the LayerSpec related color customizations.

        Color Def. Syntax :
                colorDefinitions = {  'Calculator1' :
                                             { 'geometryColor': [r,g,b],
                                               'colorLut'     : LookupTable()  }  },
                                      'Clip1' : { ... }  }
        '''
        colorDefs = dict()
        rootChildren = self._rootItem._childItems

        for visItem in rootChildren:
            if visItem.isVisible():
                 # The layout of a color table struct is in LayerSpec.py
                 colorDefs[visItem._dbParameterName] = \
                 { "colorLut" : visItem.propertyItem().colorLut(),                          \
                 "geometryColor" : visItem.propertyItem().geometryColor() }

        self.__colorDefinitions = colorDefs

    def getQuery(self):
        return self._query

    def getColorDefinitions(self):
        return self.__colorDefinitions

    @QtCore.Slot(("QModelIndex"))
    def onViewClicked(self, index):
        #layerIdx = self.index(index.row(), 0, index.parent()) # index creation using ::index
        #layerName = self.data(layerIdx, Qt.DisplayRole)
        #print "Clicked: (", index.row(), ", ", index.column(), ", ", layerName, ")"

        # visibility column
        if index.column() == 1:
            item = index.internalPointer()

            if item.isVisibilitySelectionEnabled():

                # toggle current visibility
                currentVisibility = item.toggleVisibility()
                if currentVisibility:
                    self.setData(index, "Yes", Qt.DisplayRole)
                    self.__updateColorDefinitions()
                else:
                    self.setData(index, "No", Qt.DisplayRole)

                # handle radio button behavior
                if currentVisibility and item._radioButtonBehavior:
                    siblings = item.parent()._childItems
                    row = 0
                    for s in siblings:
                        if s is not item:
                            s._visibility = False
                            sIndex = self.index(row, index.column(), index.parent())
                            self.setData(sIndex, "No", Qt.DisplayRole)
                        row += 1

                self._initializeQuery()
                self.queryChanged.emit()

    @QtCore.Slot(("QModelIndex"), ("QModelIndex"))
    def onSelectedItemChanged(self, currentIndex, previousIndex):
        item = currentIndex.internalPointer()
        propItem = item.propertyItem()

        if propItem == None: #set 'None' if properties are disabled in this item
            clutName = "None"
            valRange = tuple([None, None])
        else:
            clutName = propItem.colorLut().name
            valRange = propItem.valueRange()

        self.currentColorLutChanged.emit(clutName)
        self.valueRangeChanged.emit(valRange)
        self.itemVisibilityChanged.emit((item.parent() == self._rootItem))

    def setColorLut(self, index, lut):
        propItem = index.internalPointer().propertyItem()
        if propItem == None:
            return

        propItem.setColorLut(lut)
        self.__updateColorDefinitions()

    def setGeometryColor(self, index, color):
        propItem = index.internalPointer().propertyItem()
        if propItem == None:
          return

        propItem.setGeometryColor(color)
        self.__updateColorDefinitions()
