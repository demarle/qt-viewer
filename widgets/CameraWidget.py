#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
import PIL

from widgets.DisplayWidget import *
import cinema_python.images.querymaker as querymaker
import cinema_python.images.querymaker_specb as querymaker_specb


class CameraWidget(DisplayWidget):
    ''' Widget which contains all the necessary instances to display a Cinema
    data-base.

    Requires a Cinema store to be set at initialization. It receives queries
    from ControllersWidget which are transformed into a series of LayerSpec
    instances (by means of a QueryMaker).'''

    cameraSelected = Signal(int)
    def __init__(self, parent):
        super(CameraWidget, self).__init__(parent)
        self.__translator = None
        self.__id = None
        self._ui._renderView.mousePressSignal.connect(self.__onPressSignal)

    @QtCore.Slot()
    def __onPressSignal(self):
        ''' Forwards a signal about its selection to the parent widget (emitted
        on a mouse press event).'''
        self.cameraSelected.emit(self.__id)

    def __render(self, layers, colorDefinitions):
        ''' Computes and displays a composited image.'''
        try:
            self.compositor.setColorDefinitions(colorDefinitions)
            compositeImage = self.compositor.render(layers)
        except IndexError:
            # Handles the case when there are no layers selected (Compositor throws)
            # or layers were not loaded correctly
            print "Warning: No Layers were selected or Layers failed to load!"
            self.renderView().clear()
            return

        # Pass the result to the viewport widget
        pimg = PIL.Image.fromarray(compositeImage)
        try:
            imageString = pimg.tostring('raw', 'RGB')
        except Exception:
            # Pillow for OS X has replaced "tostring" with "tobytes"
            imageString = pimg.tobytes('raw', 'RGB')

        qimg = QImage(imageString, pimg.size[0], pimg.size[1], pimg.size[0]*3,\
          QImage.Format_RGB888)
        pix = QPixmap.fromImage(qimg)

        # Try to resize the display widget
        self.renderView().sizeHint = pix.size
        self.renderView().setPixmap(pix)

    @QtCore.Slot()
    def update(self, query, colorDefinitions):
        ''' Translates a query and passes LayerSpecs to the compostior.'''
        layers = self.__translator.translateQuery(query)
        self.__render(layers, colorDefinitions)

    def setId(self, Id):
        self.__id = Id

    def getId(self):
        return self.__id

    def setStore(self, store, spec):
        ''' Initialization. '''
        if (spec is CinemaSpec.A):
            self.__translator = querymaker.QueryMaker_SpecA()
        elif (spec is CinemaSpec.B):
            self.__translator = querymaker_specb.QueryMaker_SpecB()
        super(CameraWidget, self).setSpecification(spec)

        self.__translator.setStore(store)
