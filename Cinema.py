#!/usr/bin/python

#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
## @description Cinema viewer application. Supports loading multiple databases
#  simultaneously. Either single or a multi-database directories can be loaded
#  directly from the file menu or by passing the top info.json as an argument.
#
##  Single database
#  Pass the database's info.json path as an argument.
#
##  Multiple databases
#  Pass the top-directory's info.json path as an argument. The top-directoy
#  info.json is expected to have the following format:
#
#  {
#     "metadata": {
#         "type": "workbench"
#     },
#     "runs": [
#         {
#         "title": "image_0",
#         "description": "image_0",
#         "path": "image_0"
#         },
#         {
#         "title": "image_1",
#         "description": "image_1",
#         "path": "image_1"
#         }
#     ]
# }
#
# Where "path" is the name of the directory containing the info.json of a
# particular database.

import sys
import os

from PySide.QtCore import *
from PySide.QtGui import *

if os.path.exists("./widgets/uiFiles"):
  # compile ui files as long as the dir exists
  import pysideuic as uic
  uic.compileUiDir("./widgets/uiFiles")

from MultiViewerMain import *
from common import DatabaseLoader


# ---------------------------------------------------------------------
# create the qt app
a = QApplication(sys.argv)
v = ViewerMain()

# parse arguments if any
argv = sys.argv
if len(argv) > 1:
    databases, links = DatabaseLoader.loadAll(sys.argv[1])
    v.setStores(databases, links)

## ------------------- start profiling -------------------------------
#import cProfile, pstats, StringIO
#pr = cProfile.Profile()
#pr.enable()
## -------------------------------------------------------------------

v.show()
qAppRet = a.exec_()

## --------------------- finish profiling ----------------------------
#pr.disable()
#s = StringIO.StringIO()
#sortby = 'cumulative'
#ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
#ps.print_stats(0.1) # print 10% of calls
#print s.getvalue()
## -------------------------------------------------------------------

sys.exit(qAppRet)
